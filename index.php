

<!DOCTYPE html>
<html>
<head>
	<title></title>
	<meta charset="utf-8">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="css/font-awesome.css">
	<meta name="viewport" content="width-device-width, initial-scale=1. shrink-to-fit=no">
</head>
<body class="corps ">
	<div class="container-fluid ">
	   <div class="row" >
            <?php 

            if (isset($_SESSION['USER'])) {
                header('location:pages/header_account.php');
            }else{ ?>
                <div class="col-md-12 col-sm-12 col-xs-12  ">
                    <div class="navbar ">
                        <a href="index.php"><span class="navbar-brand logo"><strong>Find-It Group</strong></span></a>
                        <a href="index.php" style="color: white;" class="btn btn-info btn-success pull pull-right connexion">ACCUEIL</a>
                        <a href="pages/formulaire.php" style="color: white;" class="btn btn-info btn-success pull pull-right connexion">INSCRIPTION</a> 
                        <a href="pages/connexion.php" style="color: white;" class="btn btn-info btn-success pull pull-right connexion ">CONNEXION</a>
                    </div>
                </div>
        </div>
            <?php } ?>
</body>
</html>