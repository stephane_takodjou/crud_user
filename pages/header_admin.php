<?php session_start(); ?>




<!DOCTYPE html>
<html>
<head>
	<title></title>
	<meta charset="utf-8">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../css/style.css">
	<link rel="stylesheet" type="text/css" href="../css/style_account.css">
	<link rel="stylesheet" type="text/css" href="../css/font-awesome.css">
	<meta name="viewport" content="width-device-width, initial-scale=1. shrink-to-fit=no">
	<style type="text/css">
		
		ul li {
    	list-style-type: none;
   		 margin-top: 10px;
		}

		a{
		    text-decoration: none;
		}

		.nom{
		    font-weight: bold;
		    padding-top: 40px;
		    padding-right:20px;
		}

		.profil{
		    height: 50px;
		    width: 50px;
		    border-radius: 50%;
		    margin-right: 70px;
		}
		table{
			vertical-align: middle;
			background-color: gray;
		}

	</style>
	
</head>
<body >
	<?php if (isset($_SESSION['ADMINISTRATEUR'])) {?>
	<div class="container">
		<div class=" navbar  ">
                <div class="col-md-12 col-sm-12 col-xs-12  ">
                <div class="navbar">
                    <a href="../index.php"><span class="navbar-brand logo"><strong>Find-It Group</strong></span></a>
                <div class=" navbar-collapse pull pull-right" >
			    	<ul class="navbar-nav">
				      	<li><span class="nom"> <?php echo $_SESSION['ADMINISTRATEUR']['nom']." ".$_SESSION['ADMINISTRATEUR']['prenom'] ?></span></li>
				      	<li class="nav-item dropdown">
				       		<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				          		<?php echo "<img class='profil' src='../images/".$_SESSION['ADMINISTRATEUR']['photo']."'>" ?>
				        	</a>
					        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
					        	<div class="dropdown-divider"></div>
					          	<a class="dropdown-item" href="profil_admin.php">Mon profil</a>
					          	<div class="dropdown-divider"></div>
					          	<a class="dropdown-item" href="deconnexion.php">Deconnexion</a>
					          	
					        </div>
				      	</li>
				    </ul>
			    </div>
                </div>
            </div>
		
	</div>

	<h1 style="color: green; text-align: center;padding-bottom: 50px;">Liste des personnes deja inscrites sur la plateforme</h1>


		<div class="table-responsive-md table-responsive-sm table-responsive-lg ">
			<table class="table table-bordered table-striped">
			  <thead class="thead-light" >
			    <tr>
			      <th scope="col">N°</th>
			      <th scope="col" class="photo_profil">photo de profil</th>
			      <th scope="col">Nom</th>
			      <th scope="col">Prénom</th>
			      <th scope="col" class="email">E-mail</th>
			      <th scope="col" >Etat</th>
			      <th scope="col" >Actions</th>
			    </tr>
			  </thead>
			  <tbody>
			    <?php
			    	$bdd= new PDO('mysql:host=localhost;dbname=users','root','', array(PDO::ATTR_ERRMODE=>PDO::ERRMODE_EXCEPTION)); 
			    	$response = $bdd->query('SELECT  * FROM utilisateur WHERE niveau!=5');
			    	$user = array();
			    			$i=1;
			    			while ($donnees=$response->fetch()) {
			    				$user[$i] = $donnees; ?>

						  		<tr>
						  			<td><?php echo $i ; ?></td> 
						    		<td class="photo_profil"> <img src="../images/<?php echo $donnees['photo']; ?>" style=" width: 40px; height: 40px;border-radius: 50%;"</td>
						  			<td> <?php echo $donnees['nom'] ; ?></td> 
						  			<td> <?php echo $donnees['prenom'] ; ?></td>
						  			<td class="email"> <?php echo $donnees['email'] ; ?></td>
						  			<td ><?php if ($donnees['niveau'] == 1) { ?>
										<span style='color:green;font-weight:800;'> actif <span>
									<?php } else if ($donnees['niveau'] == 2) {  ?>
										<span style='color:orange;font-weight:800;'> inactif <span>
									<?php } else { ?>
										<span style='color:red;font-weight:800;'> deleted <span>
									<?php  } ?>
									
									</td>
						  			<td >
						  				<div style="font-size: 1px;"> 
											<form enctype="multipart/form-data" method="post" action="Management.php" style=" display: inline-block;">
												<a href="#">
													<input type="hidden" name="N1"  value="<?php echo $user[$i]['id']; ?>">
													<input type="hidden" name="N2"  value="consulter">
													<button  type="submit" style="background-color:inherit; border: none;">
														<span class="fa fa-eye editer" style="color:aqua; font-size: 12px;"> </span>
													</button>
												</a>
											</form>
											<form enctype="multipart/form-data" method="post" action="Management.php" style=" display: inline-block;">
												<a href="#">
													<input type="hidden" name="N1"  value="<?php echo $user[$i]['id']; ?>">
													<input type="hidden" name="N2"  value="editer">
													<button  type="submit" style="background-color:inherit; border: none;">
														<span class="fa fa-pencil editer" style="color:blue; font-size: 12px;">  </span>
													</button>
												</a>
											</form>
											<?php if($donnees['niveau'] <=2 ){ ?>
											<form enctype="multipart/form-data" method="post" action="Management.php" style=" display: inline-block;">
												<a href="#">
													<input type="hidden" name="N1"  value="<?php echo $user[$i]['id']; ?>">
													<input type="hidden" name="N2"  value="supprimer">
													<button  type="submit" style="background-color:inherit; border: none;">
														<span class="fa fa-trash Supprimer" style="margin-left: 10px;color:red; font-size: 12px;">  </span>
													</button>
												</a>
											</form>
											<?php } if ($donnees['niveau'] == 1) { ?>
								      		<form enctype="multipart/form-data" method="post" action="Management.php" style=" display: inline-block;">
								      			<a href="#">
								      				<input type="hidden" name="N1"  value="<?php echo $user[$i]['id']; ?>">
								      				<input type="hidden" name="N2"  value="desactiver">
								      				<button  type="submit" style="background-color:inherit; border: none;">
								      					<span class="glyphicon glyphicon-remove-circle activer_inverse" style="margin-left: 10px;color:orange; font-size: 12px;"> </span>
								      				</button>
								      			</a>
								      		</form>
											<?php	} else { ?>
										   <form enctype="multipart/form-data" method="post" action="Management.php" style=" display: inline-block;">
										  		<a href="#">
										  			<input type="hidden" name="N1"  value="<?php echo $user[$i]['id']; ?>">
										  			<input type="hidden" name="N2"  value="activer">
										  			<button  type="submit" style="background-color:inherit; border: none;">
										  				<span class="glyphicon glyphicon-ok activer" style="margin-left: 10px;color:green; font-size: 12px;"> </span>
										  			</button>
										  		</a>
										   </form>
							      		</div>

						  			</td>
						    		</tr>
							  	
								<?php } ?>
								<?php $i++ ; } ?>



			    		
			              
			     
			  </tbody>
			</table>
		</div>

	</div>

	<script type="text/javascript" src="../javascript/jquery-3.6.0.min.js"></script>
	<script type="text/javascript" src="../bootstrap/js/bootstrap.min.js"></script>

<?php } ?>