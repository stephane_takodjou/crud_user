<?php session_start(); ?>

<!DOCTYPE html>
<html>
<head>
	<title></title>
	<meta charset="utf-8">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../css/style.css">
	<link rel="stylesheet" type="text/css" href="../css/style_account.css">
	<link rel="stylesheet" type="text/css" href="../css/font-awesome.css">
	<meta name="viewport" content="width-device-width, initial-scale=1. shrink-to-fit=no">
	<style type="text/css">
		
		ul li {
    	list-style-type: none;
   		 margin-top: 10px;
		}

		a{
		    text-decoration: none;
		}

		.nom{
		    font-weight: bold;
		    padding-top: 40px;
		    padding-right:20px;
		}

		.profil{
		    height: 50px;
		    width: 50px;
		    border-radius: 50%;
		    margin-right: 70px;
		}
		table{
			vertical-align: middle;
			background-color: gray;
		}

	</style>
	
</head>
<body >
	<div class="container">
		<div class=" navbar  ">
                <div class="col-md-12 col-sm-12 col-xs-12  ">
                <div class="navbar">
                    <a href="../index.php"><span class="navbar-brand logo"><strong>Find-It Group</strong></span></a>
                <div class=" navbar-collapse pull pull-right" >
			    	<ul class="navbar-nav">
				      	<li><span class="nom"> <?php echo $_SESSION['ADMINISTRATEUR']['nom']." ".$_SESSION['ADMINISTRATEUR']['prenom'] ?></span></li>
				      	<li class="nav-item dropdown">
				       		<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				          		<?php echo "<img class='profil' src='../images/".$_SESSION['ADMINISTRATEUR']['photo']."'>" ?>
				        	</a>
					        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
					        	<div class="dropdown-divider"></div>
					          	<a class="dropdown-item" href="profil_admin.php">Mon profil</a>
					          	<div class="dropdown-divider"></div>
					          	<form method="post" action="deconnexion.php">
					          		<a class="dropdown-item" href="connexion_admin.php">Deconnexion</a>
					          	</form>
					        </div>
				      	</li>
				    </ul>
			    </div>
                </div>
            </div>
		
	</div>
<div class="col-md-offset-3 col-md-6 col-sm-10 col-xs-12 form_connexion" style="text-align: center;" >
	<h3>Mon profil</h3>
	<p> <?php echo "<img class='profil' src='../images/".$_SESSION['ADMINISTRATEUR']['photo']."'>" ?></p><br>
	<p for="nom">Nom :<?php echo $_SESSION['ADMINISTRATEUR']['nom'] ?></p>
	<p for="prenom">Prenom :<?php echo $_SESSION['ADMINISTRATEUR']['prenom'] ?></p>
	<p for="email">Email :<?php echo $_SESSION['ADMINISTRATEUR']['email'] ?></p>
	<!-- <p for="pass">Mot de passe actuel :<?php echo $_SESSION['ADMINISTRATEUR']['pwd'] ?></p><br> -->
	<button data-toggle="modal" href="#info" class="pull pull-right btn btn-success glyphicon glyphicon-pencil">Modifier</button>
</div>
<div class="modal fade  " id="info" >
	<div class="modal-dialog">
		<div class="modal-content form1">
			<div class="modal-header">
				<button class="close btn btn-danger" data-dismiss="modal">Fermer</button>
				<div class="modal-title">
					<h2>Modifiez vos informations </h2>
				</div>
			</div>
			<div class="modal-body">
				<form action="modification.php" method="post" enctype="multipart/form-data" >
					<label for="nom">Nom :</label>
					<input id="nom" class="form-control" type="text" name="nom" value="<?php echo $_SESSION['ADMINISTRATEUR']['nom'] ?>" required>
					<label for="prenom">Prenom :</label>
					<input id="prenom" class="form-control" type="text" name="prenom" value="<?php echo $_SESSION['ADMINISTRATEUR']['prenom'] ?>" required>
					<label for="email">Email :</label>
					<input id="email" class="form-control" type="text" name="email" value="<?php echo $_SESSION['ADMINISTRATEUR']['email'] ?>" required>
					<label for="pass">Mot de passe actuel :</label>
					<input id="pass" class="form-control" type="password" name="pwd1" value="<?php echo $_SESSION['ADMINISTRATEUR']['pwd'] ?>">
					<label for="pass1">Nouveau Mot de passe :</label>
					<input id="pass1" class="form-control" type="password" name="pwd2" placeholder ="Entrez votre nouveau mot de passe" required>
					<label>Telecharger votre nouvelle photo de pofil</label>
					<input class="form-control inpt7" type="file" name="photo" accept="image/*" onchange="loadFile(event)" required="" >
					    	<img id="im"/>
							<input  type="submit"  class="btn  btn-info btn-success" value="Modifier">
						<!-- </div> -->
				</form>
			</div>
		</div>
	</div>
</div>

	<script type="text/javascript">
		var loadFile = function(event) {
		var profil = document.getElementById('im');
		profil.src = URL.createObjectURL(event.target.files[0]);
		};
	</script>

	
	<script type="text/javascript" src="../javascript/jquery-3.6.0.min.js"></script>
	<script type="text/javascript" src="../bootstrap/js/bootstrap.min.js"></script>