<?php session_start();?>

<!DOCTYPE html>
<html>
<head>
	<title></title>
	<meta charset="utf-8">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../css/style.css">
	<link rel="stylesheet" type="text/css" href="../css/style_account.css">
	<link rel="stylesheet" type="text/css" href="../css/font-awesome.css">
	<meta name="viewport" content="width-device-width, initial-scale=1. shrink-to-fit=no">
	<style type="text/css">
		
		ul li {
    	list-style-type: none;
   		 margin-top: 10px;
		}

		a{
		    text-decoration: none;
		}

		.nom{
		    font-weight: bold;
		    padding-top: 40px;
		    padding-right:20px;
		}

		.profil{
		    height: 70px;
		    width: 70px;
		    border-radius: 50%;
		    margin-right: 70px;
		}
	</style>
	
</head>
<body>
	<?php 
		if (isset($_SESSION['USER'])) {
			
		
	 ?>
	<div class="container">
		<div class=" navbar ">
                <div class="col-md-12 col-sm-12 col-xs-12  ">
                    <a href="../index.php"><span class="navbar-brand logo"><strong>Find-It Group</strong></span></a>
                <div class=" navbar-collapse pull pull-right" >
			    	<ul class="navbar-nav">
				      	<li><span class="nom"> <?php echo $_SESSION['USER']['nom']." ".$_SESSION['USER']['prenom'] ?></span></li>
				      	<li class="nav-item dropdown">
				       		<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				          		<?php echo "<img class='profil' src='../images/".$_SESSION['USER']['photo']."'>" ?>
				        	</a>
					        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
					        	<div class="dropdown-divider"></div>
					          	<a class="dropdown-item" href="profil.php">Mon profil</a>
					          	<div class="dropdown-divider"></div>
					          	<a class="dropdown-item" href="deconnexion.php">Deconnexion</a>
					          	
					        </div>
				      	</li>
				    </ul>
			    </div>
            </div>
		</div>

<script type="text/javascript" src="../javascript/jquery-3.6.0.min.js"></script>
<script type="text/javascript" src="../bootstrap/js/bootstrap.min.js"></script>

	<?php  } ?>
