

<?php
    session_start(); 
	
    if (isset($_SESSION['USER'])) {
        header('location:header_account.php');
    }else{ 
        include ("header.php");
    ?>
        
            <div class="col-md-offset-3 col-md-6 form_connexion">
                <form method="post" action="traitementconnexion.php" enctype="multipart/form-data">
                    <h3>Connectez-vous</h3>
                   <p style="color: red; text-align: center;font-weight: bold;"> <?php if (isset($_SESSION['message'])) {
                        echo $_SESSION['message'];
                    } ?>
                    </p> 
                      <span class="glyphicon glyphicon-envelope"></span>
                            <label>Email</label>
                            <input class="form-control inpt3" type="email" name="email" placeholder="Veuillez entrer votre adresse mail" id="mail" required="">
                            <p></p>
                            <span class="glyphicon glyphicon-lock"></span>
                            <label>Mot de passe</label>
                            <input class="form-control inpt4" type="password" name="pwd" placeholder="Veuillez entrer votre mot de passe" id="pwd"  required=""><br>

                       
                    <div class="row">
                        <div class="col-md-offset-2 col-md-4 col-xm-5 col-xs-6  ">
                            <button type="reset" class="btn btn-block btn-info btn-success">
                                <span class="glyphicon glyphicon-remove pull pull-left"></span>
                                <strong>ANNULER</strong> 
                            </button>
                        </div>
                        <div class="col-md-4 col-xm-5 col-xs-6">
                            <button type="submit" class="btn btn-block btn-info btn-success	 " value="connexion">
                                <span class="glyphicon glyphicon-saved pull pull-left"></span>
                                <strong>SE CONNECTER</strong>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
<?php } ?>
<?php session_destroy(); ?>
