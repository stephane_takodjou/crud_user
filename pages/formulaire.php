

 <?php if (isset($_SESSION['USER'])) {
 	header('location:header_account.php');
 }else{

 	include ("header.php");
  ?>

	<div class="container-fluid ">
		
		<div class="row ">
			<h2></h2>
			<div class=" form1 col-md-offset-2 col-md-8  col-xs-12 col-xm-offset-2 col-xm-10">
				<h3>INSCRIVEZ VOUS</h3>
				<p style="color: red;font-weight: bold;text-align: center;"><?php 

					if ( isset($_SESSION['message_error'])) {
						echo $_SESSION['message_error'];
					}

				 ?></p>
				<form enctype="multipart/form-data" method="post" action="traitement.php" id="myform" >
					<div class="row">
						<div class="col-md-5 col-xm-5 col-xs-offset-1 col-xs-11 form2">
							<span class="glyphicon glyphicon-user"></span>
							<label>Noms</label>
							<input id="name" class=" form-control inpt1 " type="text" name="nom" placeholder="veuillez entrer votre nom"  required="">
							<p></p>
							<span class="glyphicon glyphicon-user"></span>
							<label>Prenoms</label>
							<input class="form-control inpt2" type="text" name="prenom" placeholder="veuillez entrer votre nom"  id="prenom" required="">
							<p></p>
							<span class="glyphicon glyphicon-envelope"></span>
							<label>Email</label>
							<input class="form-control inpt3" type="email" name="email" placeholder="veuillez entrer votre adresse mail" id="mail" required="">
							<p></p>
							<span class="glyphicon glyphicon-lock"></span>
							<label>Mot de passe</label>
							<input class="form-control inpt4" type="password" name="pwd" placeholder="Veuillez entrer votre mot de passe" id="pwd"  required=""><br>
							<input type="hidden" name="niveau" value="1">
						</div>
						<div class="col-md-offset-1 col-md-5 col-md-pull-1 col-xm-5 col-xs-offset-1 col-xs-11 form3" style="margin-top: 100px;">
								
							<span class="glyphicon glyphicon-picture"></span>
							<label>Telecharger votre photo de pofil</label>
							<input class="form-control inpt7" type="file" name="photo" accept="image/*" onchange="loadFile(event)" required="" >
							<div class="col-md-offset-4 col-md-4" style="">
	                        	<img id="im"/>
	                  		</div>
						</div>
					</div>
					<div class="col-md-offset-2 col-md-4 col-xm-5 col-xs-6 ">
						<input  type="reset"  class="  btn btn-block btn-info btn-success"> 
						
					</div>
					<div class="  col-md-4 col-xm-5 col-xs-6">
						<input  type="submit"  class="  btn btn-block btn-info btn-success" value="S'inscrire">
					</div>
						
				</form>		
			</div>
		</div>
		
	</div>
<?php } ?>

	
	<!-- <script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script> -->
	<script type="text/javascript">
    var loadFile = function(event) {
        var profil = document.getElementById('im');
        profil.src = URL.createObjectURL(event.target.files[0]);
      };


<?php session_destroy(); ?>

  </script>
  
</body>

</html>